
var app = angular.module("yjApp", []);

app.controller("yjAppController", function($scope, $http){
	
	$scope.user = new User();
	
	$scope.req = function(type,ob,cb){
		$http({
			url : type,
			method : 'GET',
			params : ob
		}).then(function(d){
			cb(d.data, d);
		});
	};
	
	$scope.isLoggedIn = function(){
		$scope.req('isLoggedIn', {}, function(d){
			$scope.user.data = d.user;
			$scope.user.loggedIn = d.isLoggedIn;
			console.log(d);
		});
	};
	
	$scope.logout = function(){
		$scope.req('logout', {}, function(d){
			$scope.isLoggedIn();
		});
	};
	
	$scope.isLoggedIn();
	
	$scope.userAccess = {
		signIn : false,
		un : "",
		pw : "",
		cpw : "",
		email : "",
		go: function(){
			if(this.signIn) this.signin();
			else this.signup();
		},
		signup : function(){
			var input = {un:this.un, pw: this.pw, email : this.email};
			$scope.req('signup', input, function(d){
				console.log(d);
				$scope.isLoggedIn();
				$scope.user.view = 'home';
			});
		},
		signin : function(){
			var input = {un:this.un, pw: this.pw, email : this.email};
			$scope.req('signin', input, function(d){
				console.log(d);
				$scope.isLoggedIn();
				$scope.user.view = 'home';
			});
		},
		msg : function(){
			return this.signIn ? 'Sign-In' : 'Sign-Up';
		}
	};
	
	
});


app.directive('yjFileInput', function(){
	return {
		restrict : 'AE',
		replace : 'true',
		template : '<input id="formData001" type="file" />',
		link: function($scope, $elem, $attrs){
			$elem.bind('change', function(){
				$scope.user.newdp($elem, function(){
					$scope.isLoggedIn();
				});
				
			});
		}
	};
});

