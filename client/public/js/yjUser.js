function User(){
	this.data = undefined;
	this.loggedIn = false;
	this.view = 'home';
	this.sub = '';
}

User.prototype.okay = function(){ return this.loggedIn && this.data !== undefined; };

User.prototype.dp = function(){
	if(this.okay()){
		return this.data.dp === undefined ? 'res/dps/default.png' : this.data.dp;
	}
};

User.prototype.dpFriendly = function(){
	var rv = this.dp();
	rv = rv.split('/');
	rv = rv[rv.length-1];
	rv = rv.split('.');
	return rv[0];
};

User.prototype.newdp = function(e, cb){
	var fd = new FormData();
	fd.append('file', e[0].files[0]);
	console.log(e[0].files[0]);
	var me = this;
	//fd.append("label", "WEBUPLOAD");
	$.ajax({
		url: "/newdp",
		type: "POST",
		data: fd,
		processData: false,  // tell jQuery not to process the data
		contentType: false   // tell jQuery not to set contentType
	}).done(function( data ){
		if(!data.err){
			cb();
		}
		
	});
};