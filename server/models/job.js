/*
Code for the Job model
*/
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

/*
	category [update: have drop down list]
	
*/

var job = new Schema({
	owner : {type: Schema.ObjectId, ref: 'UserSchema'},
	name : String,
	description : String,
	category : String,
	type: String,
	pay : String,
	videos : String,
	stories : Object,
	pics : String, //can be categorised as memes
	links : Array
});

module.exports = mongoose.model('Job', job);