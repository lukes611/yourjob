var config = require('../config/config'),
express = require('express'),
morgan = require('morgan'),
compress = require('compression'),
body_parser = require('body-parser'),
method_override = require('method-override'),
session = require('express-session');

module.exports = function(){
	var app = express();
	if(config.devType == 'prod')
		app.use(compress());
	
	
	app.use(body_parser.urlencoded(
	{
		extended : true
	}));
	
	app.use(body_parser.json());
	app.use(method_override());
	
	app.use(session({
		saveUninitialized : true,
		resave : true,
		secret : config.session_secret
	}));
	
	require('./routes')(app);
	
	app.use(express.static('./client/public'));
	
	return app;
};