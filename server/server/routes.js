var fs = require('fs');
var User = require('./../models/user.js');
var qs = require('querystring');
var formidable = require('formidable');
var help = require('./helpers');

module.exports = function(app){
	
	
	
	
	//for users, get their data
	app.use(help.autoLogin);
	
	app.get('/', function(req, res){
		fs.readFile(__dirname + '/../../client/public/html/index.html', function(err, data){
			if(err)
				res.send('sorry cannot find index.html');
			else
				res.send(data.toString());
		});
	});
	
	app.get('/signup', function(req, res){
		var newUser = new User(req.query);
		newUser.save(function(error, nu){
			if(error){
				res.json({err: true, msg: 'some-worries'});
			}else{
				req.session.userId = nu._id;
				res.send({err: false});
			}
		});
	});
	
	app.get('/signIn', function(req, res){
		User.findOne({pw : req.query.pw, $or : [{un:req.query.un}, {email:req.query.un}]}, function(err, usr){
			if(err || usr === undefined) res.json({err : true});
			else{
				req.session.userId = usr._id;
				res.send({err : false});
			}
		})
	});
	
	app.get('/isLoggedIn', function(req, res){
		res.json({isLoggedIn:req.isLoggedIn, user: req.user});
	});
	
	app.get('/logout', function(req, res){
		console.log('logout: ' + req.user);
		req.session.destroy();
		res.json({err : false});
	});
	
	app.post('/newdp', function(req, res){
		help.handlePost(req, function(data){
			var dataInfo = {
				uploadedPath : data.files.file.path,
				type : data.files.file.type,
				name : data.files.file.name,
				imgType : data.files.file.type.split('/').pop()
			};
			var dpName = req.user.un + Math.random() + 'dp.' + dataInfo.imgType;
			var dpPath = __dirname + '/../../client/public/res/dps/' + dpName;
			var newdp = '/res/dps/' + dpName;

			fs.rename(dataInfo.uploadedPath, dpPath, function(err){
				User.newDp(req.user, newdp, function(err){
					if(!err) res.json({err: false});
					else res.json({err: true});	
				});
			});
		});
	});
	
};
