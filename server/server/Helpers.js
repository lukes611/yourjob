var fs = require('fs');
var User = require('./../models/user.js');
var qs = require('querystring');
var formidable = require('formidable');

exports.handlePost = function(request, cb){
	var form = new formidable.IncomingForm();
	
	form.parse(request, function(err, fields, files){
		if(err){
			console.log('error occurred');
			return;
		}
		cb({
			fields : fields,
			files: files
		});
	});
};


exports.autoLogin = function(req, res, next){
	req.isLoggedIn = false;
	if(req.session.userId){
		User.findById(req.session.userId, {pw: 0}, function(error, user){
			if(!error){
				req.isLoggedIn = true;
				req.user = user;
			}
			next();
		});
	}else next();
	
};